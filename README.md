# README #

This README simply explains the Matrix_Multiplication_Worker project that is written with Pure Jdk 8.

### What is this repository for? ###

* This is the worker part of distributed matrix multiplication assignment. This worker simply opens a UDP port and waits for tasks to arrive and pushes back the results when calculation is finished. The worker consists of three main threads; where first one is for retrieving tasks via UDP socket, second one is for processing retrieved tasks, and the last one is for sending task results constructed. Other than these threads; every single task result is sent back to the task distributor in a separate thread. Moreover during actual matrix multiplication task; parallel stream of Java 8 is leveraged so as to provide more multi-threading as well.

* Version: 1.0-SNAPSHOT

### How do I get set up? ###

* Checkout the source code and just execute the Application main class. You need to pass port number to receive matrix multiplication tasks via UDP protocol.
* You can also run the unit tests.
* The project has no dependencies other than Mockito and Junit libraries; which are only used for testing purposes.
* The repository also contains an executable jar; "matrix_multiplication_worker.jar". You can run the jar via "java -jar matrix_multiplication_worker.jar <port-number>" command with jre 8.

### Contribution guidelines ###

* No one is expected to contribute to this repository other than owner.

### Who do I talk to? ###

* Repo owner.