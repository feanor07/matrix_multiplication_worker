package com.dilek.alptug.taskinformation;

import static org.junit.Assert.*;

import com.dilek.alptug.taskresult.InMemoryTaskResultRepository;
import org.junit.Test;

import com.dilek.alptug.entity.TaskResult;

/**
 * Test class for {@link InMemoryTaskResultRepository}
 *
 * @author alptugd
 */
public class InMemoryTaskResultRepositoryTest {

	@Test
	public void testPersistAndGetResult() {
		InMemoryTaskResultRepository inMemoryTaskResultRepository = new InMemoryTaskResultRepository();
		TaskResult taskResult = new TaskResult();
		taskResult.setTaskId(99);
		inMemoryTaskResultRepository.persistResult(taskResult);
		assertNull(inMemoryTaskResultRepository.getTaskResultByTaskId(98));
		assertNotNull(inMemoryTaskResultRepository.getTaskResultByTaskId(99));
	}

}
