package com.dilek.alptug.taskinformation;

import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskInformation;
import com.dilek.alptug.entity.TaskInformation.TaskStatus;
import com.dilek.alptug.entity.TaskResult;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Test class for {@link DefaultTaskInformationBuffer}
 *
 * @author alptugd
 */
public class DefaultTaskInformationBufferTest {

	@Test
	public void testPut() {
		DefaultTaskInformationBuffer defaultWorkerInputBuffer = new DefaultTaskInformationBuffer();
		TaskInformation taskInformation = createTaskInformation(defaultWorkerInputBuffer);
		defaultWorkerInputBuffer.put(taskInformation);
		defaultWorkerInputBuffer.put(taskInformation);
        int id = 10;
        TaskInformation receivedTaskInformation = defaultWorkerInputBuffer.getTaskInformationByTaskId(id);
		assertNotNull(receivedTaskInformation);
		removeTaskById(defaultWorkerInputBuffer, taskInformation, id);
		receivedTaskInformation = defaultWorkerInputBuffer.getTaskInformationByTaskId(id);
		assertNull(receivedTaskInformation);
	}


	private void removeTaskById(DefaultTaskInformationBuffer defaultWorkerInputBuffer, TaskInformation taskInformation, int taskId) {
		taskInformation.setTaskResult(new TaskResult());
		taskInformation.setStatus(TaskStatus.TO_BE_SENT);
		defaultWorkerInputBuffer.removeTaskInformationByTaskId(taskId);
	}
	
	
	@Test
	public void testGetTaskInformationById() {
		DefaultTaskInformationBuffer defaultWorkerInputBuffer = new DefaultTaskInformationBuffer();
		TaskInformation taskInformation = createTaskInformation(defaultWorkerInputBuffer);
		defaultWorkerInputBuffer.put(taskInformation);
		TaskInformation receivedTaskInformation = defaultWorkerInputBuffer.getTaskInformationByTaskId(10);
		assertEquals(99, receivedTaskInformation.getTask().getVectorLength());
	}
	
	@Test
	public void testGetProcessableTask() {
		DefaultTaskInformationBuffer defaultWorkerInputBuffer = new DefaultTaskInformationBuffer();
		TaskInformation taskInformation = createTaskInformation(defaultWorkerInputBuffer);
		defaultWorkerInputBuffer.put(taskInformation);
		
		List<TaskInformation> processableTasks = defaultWorkerInputBuffer.getProcessableTaskInformations();
		assertFalse(processableTasks.isEmpty());
		taskInformation.setStatus(TaskStatus.IN_PROGRESS);
		processableTasks = defaultWorkerInputBuffer.getProcessableTaskInformations();
		assertTrue(processableTasks.isEmpty());
	}
	
	@Test
	public void testGetCalculatedTasks() {
		DefaultTaskInformationBuffer defaultWorkerInputBuffer = new DefaultTaskInformationBuffer();
		TaskInformation taskInformation = createTaskInformation(defaultWorkerInputBuffer);
		defaultWorkerInputBuffer.put(taskInformation);
		
		List<TaskInformation> processableTasks = defaultWorkerInputBuffer.getCalculatedTaskInformations();
		assertTrue(processableTasks.isEmpty());
		taskInformation.setTaskResult(new TaskResult());
		taskInformation.setStatus(TaskStatus.COMPLETED);
		processableTasks = defaultWorkerInputBuffer.getCalculatedTaskInformations();
		assertFalse(processableTasks.isEmpty());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testRemoveTaskInformationByTaskIdNotAllowed() {
		DefaultTaskInformationBuffer defaultWorkerInputBuffer = new DefaultTaskInformationBuffer();
		TaskInformation taskInformation = createTaskInformation(defaultWorkerInputBuffer);
		defaultWorkerInputBuffer.put(taskInformation);
		
		defaultWorkerInputBuffer.removeTaskInformationByTaskId(10);
	}
	
	@Test
	public void testRemoveTaskInformationByTaskId() {
		DefaultTaskInformationBuffer defaultWorkerInputBuffer = new DefaultTaskInformationBuffer();
		TaskInformation taskInformation = createTaskInformation(defaultWorkerInputBuffer);
		taskInformation.setTaskResult(new TaskResult());
		taskInformation.setStatus(TaskStatus.TO_BE_SENT);
		defaultWorkerInputBuffer.put(taskInformation);
		
		defaultWorkerInputBuffer.removeTaskInformationByTaskId(10);
		assertNull(defaultWorkerInputBuffer.getTaskInformationByTaskId(10));
	}

	private TaskInformation createTaskInformation(DefaultTaskInformationBuffer defaultWorkerInputBuffer) {
		Task task = new Task();
		task.setTaskId(10);
		task.setVectorLength(99);
		TaskInformation taskInformation = new TaskInformation(task);
		defaultWorkerInputBuffer.put(taskInformation);
		return taskInformation;
	}
}
