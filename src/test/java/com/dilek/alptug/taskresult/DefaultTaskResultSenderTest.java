package com.dilek.alptug.taskresult;

import com.dilek.alptug.taskinformation.TaskInformationBuffer;
import com.dilek.alptug.datasender.ObjectSender;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskInformation;
import com.dilek.alptug.entity.TaskInformation.TaskStatus;
import com.dilek.alptug.entity.TaskResult;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;

/**
 * Test class for {@link DefaultTaskResultSender}.
 *
 * @author alptugd
 */
public class DefaultTaskResultSenderTest {

	private TaskInformationBuffer buffer;
	private ObjectSender<TaskResult> udpDataSender;
	
	@Before
	@SuppressWarnings("unchecked")
	public void beforeTest() {
		buffer = Mockito.mock(TaskInformationBuffer.class);
		udpDataSender = Mockito.mock(ObjectSender.class);
	}

	@Test
	public void testNoCalculatedTaskExisting() throws InterruptedException {
		Mockito.when(buffer.getCalculatedTaskInformations()).thenReturn(Arrays.asList(new TaskInformation[]{}));
		DefaultTaskResultSender taskResultSender = new DefaultTaskResultSender(udpDataSender);
		taskResultSender.sendTaskResults(buffer);
		Thread.sleep(300);
		Mockito.verify(udpDataSender, Mockito.never()).sendObject(Mockito.any(), Mockito.any());
	}
	
	@Test
	public void testSendCalculatedTask() throws InterruptedException {
		Task task = new Task();
		task.setTaskId(99);
		task.setReplyAddress("localhost");
		task.setReplyPort(1234);
		TaskInformation taskInformation = new TaskInformation(task);
		TaskResult taskResult = new TaskResult();
		taskInformation.setTaskResult(taskResult);
		taskInformation.setStatus(TaskStatus.COMPLETED);
		
		Mockito.when(buffer.getCalculatedTaskInformations()).thenReturn(Arrays.asList(new TaskInformation[]{taskInformation}));
		DefaultTaskResultSender taskResultSender = new DefaultTaskResultSender(udpDataSender);
		Mockito.when(udpDataSender.sendObject(Mockito.any(), Mockito.any())).thenReturn(true);
		taskResultSender.sendTaskResults(buffer);
		Thread.sleep(300);
		Mockito.verify(udpDataSender, Mockito.times(1)).sendObject(Mockito.any(), Mockito.any());
		Mockito.verify(buffer, Mockito.times(1)).removeTaskInformationByTaskId(99);
	}

}
