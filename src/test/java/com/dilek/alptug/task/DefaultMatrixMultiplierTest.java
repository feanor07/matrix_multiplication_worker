package com.dilek.alptug.task;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.dilek.alptug.entity.Task;

/**
 * Test class for {@link DefaultMatrixMultiplier}
 *
 * @author alptugd
 */
public class DefaultMatrixMultiplierTest {

	@Test
	public void testMatrixMultiplication() {
		Task task = new Task();
		// [1 2 3]
		// [4 5 6]  -> two dimentional matrix 3 x 2
		task.setRowVectors(new int[]{1,2,3,4,5,6});
		//[7   8]
		//[9  10]
		//[11 12]  -> two dimentional matrix 2 x 3
		task.setColumnVectors(new int[]{7,9,11,8,10,12});
		task.setVectorLength(3);
		
		DefaultMatrixMultiplier matrixMultiplier = new DefaultMatrixMultiplier();
		int[][] resultMatrix = matrixMultiplier.multiplyMatrix(task);
		assertEquals(2, resultMatrix.length);
		assertEquals(2, resultMatrix[0].length);
		
		assertEquals(58, resultMatrix[0][0]);
		assertEquals(64, resultMatrix[0][1]);
		assertEquals(139, resultMatrix[1][0]);
		assertEquals(154, resultMatrix[1][1]);
	}
	
	@Test
	public void testMinusValuesMatrixMultiplication() {
		Task task = new Task();
		// [1 4 -3]  -> two dimentional matrix 3 x 2
		task.setRowVectors(new int[]{1,4,-3});
		//[2  3]
		//[5  4]
		//[7  9]  -> two dimentional matrix 2 x 3
		task.setColumnVectors(new int[]{2,5,7,3,4,9});
		task.setVectorLength(3);
		
		DefaultMatrixMultiplier matrixMultiplier = new DefaultMatrixMultiplier();
		int[][] resultMatrix = matrixMultiplier.multiplyMatrix(task);
		assertEquals(1, resultMatrix.length);
		assertEquals(2, resultMatrix[0].length);
		
		assertEquals(1, resultMatrix[0][0]);
		assertEquals(-8, resultMatrix[0][1]);
	}

}
