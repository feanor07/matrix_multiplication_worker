package com.dilek.alptug.task;

import com.dilek.alptug.taskinformation.DefaultTaskInformationBufferProcessor;
import com.dilek.alptug.taskinformation.TaskInformationBuffer;
import com.dilek.alptug.taskresult.TaskResultRepository;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskInformation;
import com.dilek.alptug.entity.TaskInformation.TaskStatus;
import com.dilek.alptug.entity.TaskResult;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test class for {@link DefaultTaskInformationBufferProcessor}
 */
public class DefaultTaskInformationBufferProcessorTest {

    private DefaultTaskInformationBufferProcessor taskProcessor;

    @Mock
    private TaskInformationBuffer buffer;

    @Mock
	private TaskResultRepository taskResultRepository;

    @Mock
	private MatrixMultiplier matrixMultiplier;

	private int port = 1;
	private String ip = "1.1.1.1";
	
	@Before
	public void before() {
        MockitoAnnotations.initMocks(this);
        this.taskProcessor = new DefaultTaskInformationBufferProcessor(taskResultRepository, matrixMultiplier, ip, port);
	}

	@Test
	public void testProcessTaskWhichIsCalculatedBefore() throws InterruptedException {
		Task task = new Task();
		task.setTaskId(10);
		TaskInformation taskInformation = new TaskInformation(task);
		taskInformation.setStatus(TaskStatus.NEW);
		when(buffer.getProcessableTaskInformations()).thenReturn(Collections.singletonList(taskInformation));
		
		TaskResult taskResult = new TaskResult();
		taskResult.setResult(new int[][]{{1},{1}});
		when(taskResultRepository.getTaskResultByTaskId(10)).thenReturn(taskResult);
		taskProcessor.processTasksInBuffer(buffer);
		Thread.sleep(300);
		verify(matrixMultiplier, Mockito.never()).multiplyMatrix(Mockito.any());
	}


	@Test
	public void testProcessTaskWhichIsNotCalculatedBefore() throws InterruptedException {
		Task task = new Task();
		task.setTaskId(10);
		TaskInformation taskInformation = new TaskInformation(task);
		taskInformation.setStatus(TaskStatus.NEW);
		when(buffer.getProcessableTaskInformations()).thenReturn(Collections.singletonList(taskInformation));
		
		TaskResult taskResult = new TaskResult();
		taskResult.setResult(new int[][]{{1},{1}});
		when(taskResultRepository.getTaskResultByTaskId(10)).thenReturn(null);
		
		when(matrixMultiplier.multiplyMatrix(task)).thenReturn(new int[][]{{2},{3},{4}});
		
		taskProcessor.processTasksInBuffer(buffer);
		Thread.sleep(300);
		
		Mockito.verify(matrixMultiplier, Mockito.times(1)).multiplyMatrix(task);
		assertEquals(10, taskInformation.getTaskResult().getTaskId());
		assertEquals(3, taskInformation.getTaskResult().getResult().length);
		assertEquals(1, taskInformation.getTaskResult().getResult()[0].length);
		assertEquals(2, taskInformation.getTaskResult().getResult()[0][0]);
		assertEquals(3, taskInformation.getTaskResult().getResult()[1][0]);
		assertEquals(4, taskInformation.getTaskResult().getResult()[2][0]);
		assertEquals(ip, taskInformation.getTaskResult().getReceivedFromIp());
		assertEquals(port, taskInformation.getTaskResult().getReceivedFromPort());
		
	}
}
