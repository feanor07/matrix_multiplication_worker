package com.dilek.alptug.taskinformation;

import com.dilek.alptug.taskinformation.TaskInformationBuffer;

/**
 * Functional interface responsible for executing tasks in {@link TaskInformationBuffer}
 * 
 * @author alptugd
 */
public interface TaskInformationBufferProcessor {

	void processTasksInBuffer(TaskInformationBuffer buffer);
}
