package com.dilek.alptug.taskinformation;

import java.util.List;

import com.dilek.alptug.entity.TaskInformation;

/**
 * Interface representing taskinformation for holding task information
 *
 * @author alptugd
 */
public interface TaskInformationBuffer {

	/**
	 * Puts the given task to the taskinformation
	 * 
	 * @param taskInformation, task information to be put into
	 */
	void put(TaskInformation taskInformation);
	
	/**
	 * Returns processable tasks from this taskinformation
	 * 
	 * @return processable tasks
	 */
	List<TaskInformation> getProcessableTaskInformations();
	
	/**
	 * Returns task information for given id
	 * 
	 * @param id, identifier of the task information
	 * @return task information
	 */
	TaskInformation getTaskInformationByTaskId(int id);
	
	/**
	 * Returns list of calculated task information ready to be sent
	 * 
	 * @return calculated task information
	 */
	List<TaskInformation> getCalculatedTaskInformations();
	
	/**
	 * Removes task information with the given id from the taskinformation
	 * 
	 * @param id identifier of the task information 
	 */
	void removeTaskInformationByTaskId(int id);
}
