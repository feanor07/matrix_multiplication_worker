package com.dilek.alptug.taskinformation;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.dilek.alptug.task.MatrixMultiplier;
import com.dilek.alptug.taskresult.TaskResultRepository;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskInformation;
import com.dilek.alptug.entity.TaskInformation.TaskStatus;
import com.dilek.alptug.entity.TaskResult;

/**
 * Default implementation for {@link TaskInformationBufferProcessor}
 *
 * @author alptugd
 */
public class DefaultTaskInformationBufferProcessor implements TaskInformationBufferProcessor {

	private final TaskResultRepository taskResultRepository;
	private final MatrixMultiplier matrixMultiplier;
	private final String receivedFromIp;
	private final int receivedFromPort;

	public DefaultTaskInformationBufferProcessor(TaskResultRepository taskResultRepository,
                                                 MatrixMultiplier matrixMultiplier, String receivedFromIp, int receivedFromPort) {
		this.taskResultRepository = taskResultRepository;
		this.matrixMultiplier = matrixMultiplier;
		this.receivedFromIp = receivedFromIp;
		this.receivedFromPort = receivedFromPort;
	}

	@Override
	public void processTasksInBuffer(TaskInformationBuffer buffer) {
		List<TaskInformation> processableTasks = buffer.getProcessableTaskInformations();
		processableTasks.stream().forEach(taskInformation -> {
			taskInformation.setStatus(TaskStatus.IN_PROGRESS);
			new Thread(() -> {
				Logger.getLogger(DefaultTaskInformationBufferProcessor.class.getName()).log(Level.INFO, "Task with id " + taskInformation.getTask().getTaskId() + " is being executed.");
				Task task = taskInformation.getTask();
				TaskResult taskResult = taskResultRepository.getTaskResultByTaskId(task.getTaskId());
				// check whether the task calculated before or not
				if (taskResult == null) {
					taskResult = calculateTaskResult(task);
				}
				taskInformation.setTaskResult(taskResult);
				Logger.getLogger(DefaultTaskInformationBufferProcessor.class.getName()).log(Level.INFO, "Task with id " + taskInformation.getTask().getTaskId() + " is completed");
				taskInformation.setStatus(TaskStatus.COMPLETED);
			}).start();
		});
	}

	private TaskResult calculateTaskResult(Task task) {
		int[][] result = matrixMultiplier.multiplyMatrix(task);
		TaskResult taskResult = new TaskResult();
		taskResult.setTaskId(task.getTaskId());
		taskResult.setResult(result);
		taskResult.setReceivedFromIp(receivedFromIp);
		taskResult.setReceivedFromPort(receivedFromPort);
		taskResultRepository.persistResult(taskResult);
		return taskResult;
	}
}
