package com.dilek.alptug.taskinformation;

import com.dilek.alptug.entity.TaskInformation;
import com.dilek.alptug.entity.TaskInformation.TaskStatus;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Default {@link TaskInformationBuffer} implementation that is inherently synchronized in terms of putting to and
 * removing from.
 *
 * @author alptugd
 */
public class DefaultTaskInformationBuffer implements TaskInformationBuffer {
	private Map<Integer, TaskInformation> taskInformationMap = new ConcurrentHashMap<>();

	@Override
	public void put(TaskInformation taskInformation) {
		if(!taskInformationMap.containsKey(taskInformation.getTask().getTaskId())) {
			taskInformationMap.put(taskInformation.getTask().getTaskId(), taskInformation);
		}
	}

	@Override
	public List<TaskInformation> getProcessableTaskInformations() {
		return taskInformationMap.values().stream().filter(ti->ti.getStatus().equals(TaskInformation.TaskStatus.NEW)).
				collect(Collectors.toList());
	}
	
	@Override
	public List<TaskInformation> getCalculatedTaskInformations() {
		return taskInformationMap.values().stream().filter(ti->ti.getStatus().
				equals(TaskInformation.TaskStatus.COMPLETED)).collect(Collectors.toList());
	}
	
	@Override
	public void removeTaskInformationByTaskId(int id) {
		TaskInformation taskInformation = taskInformationMap.get(id);
		if(taskInformation.getStatus() != TaskStatus.TO_BE_SENT) {
			throw new IllegalArgumentException("Tasks which which are not in TO_BE_SENT state cannot be deleted.");
		}
		taskInformationMap.remove(id);
	}
	
	@Override
	public TaskInformation getTaskInformationByTaskId(int id) {
		return taskInformationMap.get(id);
	}
}
