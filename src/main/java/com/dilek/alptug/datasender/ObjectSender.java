package com.dilek.alptug.datasender;

import java.io.Serializable;
import java.net.InetSocketAddress;

/**
 * Functional interface responsible for sending an object to the specified address.
 *
 * @author alptugd
 */
public interface ObjectSender<T extends Serializable> {
	/**
	 * This method sends object to a reciver 
	 * 
	 * @param object, object to be sent
	 * @param address, address of the receiver 
	 * @return a boolean to represent whether the send operation is successful or not
	 */
    boolean sendObject(T object, InetSocketAddress address);
}
