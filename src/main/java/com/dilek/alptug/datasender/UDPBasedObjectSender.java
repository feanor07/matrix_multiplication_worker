package com.dilek.alptug.datasender;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

/**
 * UDP protocol based {@link ObjectSender} implementation.
 *
 * @author alptugd
 */
public class UDPBasedObjectSender<T extends Serializable> implements ObjectSender<T> {

    public boolean sendObject(T object, InetSocketAddress address) {
        DatagramSocket clientSocket =  null;

        try {
            byte[] data = getByteArray(object);
            clientSocket = new DatagramSocket();
            DatagramPacket sendPacket = new DatagramPacket(data, data.length, address);
            clientSocket.send(sendPacket);
            clientSocket.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (clientSocket != null) {
                clientSocket.close();
            }
        }
    }

    private byte[] getByteArray(T object) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(object);
        oos.flush();

        return baos.toByteArray();
    }
}
