package com.dilek.alptug.task;

import com.dilek.alptug.entity.Task;

/**
 * Functional interface to perform multiplication operation for the given task.
 */
public interface MatrixMultiplier {
	int[][] multiplyMatrix(Task task);
}
