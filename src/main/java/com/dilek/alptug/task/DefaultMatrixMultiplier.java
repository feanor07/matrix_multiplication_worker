package com.dilek.alptug.task;

import java.util.Arrays;
import java.util.stream.IntStream;

import com.dilek.alptug.entity.Task;

/**
 * Default implementation for {@link MatrixMultiplier}. Note that java8 parallel stream also provides even multi-threading
 * within multiplyMatrix method.
 *
 * @author alptugd
 */
public class DefaultMatrixMultiplier implements MatrixMultiplier {

	@Override
	public int[][] multiplyMatrix(Task task) {
		int[] rowVectors = task.getRowVectors();
		int[] columnVectors = task.getColumnVectors();
		int vectorLength = task.getVectorLength();

		int resultRowCount = rowVectors.length / vectorLength;
		int resultCoumnCount = columnVectors.length / vectorLength;
		
		int[][] result = new int[resultRowCount][resultCoumnCount];
		for(int i = 0; i < resultRowCount; i++) {
			for(int j = 0; j < resultCoumnCount; j++) {
				int[] row = Arrays.copyOfRange(rowVectors, i*vectorLength, i*vectorLength + vectorLength);
				int[] column = Arrays.copyOfRange(columnVectors, j*vectorLength, j*vectorLength + vectorLength);
				int sum = IntStream.range(0, row.length).parallel().map(index->row[index] * column[index]).sum();
				result[i][j] = sum;
			}
		}
		return result;
	}

}
