package com.dilek.alptug.entity;

/**
 * This class wraps task, task result and status all together for processing.
 * 
 * @author alptugd
 *
 */
public class TaskInformation {
	/**
	 * To be executed task
	 */
	private Task task;
	/**
	 * Task status
	 */
	private TaskStatus status = TaskStatus.NEW;
	/**
	 * Result of the task
	 */
	private TaskResult taskResult;
	
	public TaskInformation(Task task) {
		this.task = task;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		if(status == TaskStatus.COMPLETED && taskResult == null) {
			throw new IllegalArgumentException("Task status cannot be set to COMPLETED when task result is null.");
		}
		this.status = status;
	}

	public TaskResult getTaskResult() {
		return taskResult;
	}

	public void setTaskResult(TaskResult taskResult) {
		this.taskResult = taskResult;
	}
	
	public enum TaskStatus {
		NEW, IN_PROGRESS, COMPLETED, TO_BE_SENT
	}
}
