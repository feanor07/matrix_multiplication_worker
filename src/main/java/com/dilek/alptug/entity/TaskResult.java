package com.dilek.alptug.entity;

import java.io.Serializable;

/**
 * This class represents a task result to be sent from workers as a response to an offloaded task.
 *
 * @author alptugd
 */
public class TaskResult implements Serializable {
    private int taskId;
    private int[][] result;
    private int receivedFromPort;
    private String receivedFromIp;

    public void setReceivedFromIp(String receivedFromIp) {
        this.receivedFromIp = receivedFromIp;
    }

    public String getReceivedFromIp() {
        return receivedFromIp;
    }

    public void setReceivedFromPort(int receivedFromPort) {
        this.receivedFromPort = receivedFromPort;
    }

    public int getReceivedFromPort() {
        return receivedFromPort;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int[][] getResult() {
        return result;
    }

    public void setResult(int[][] result) {
        this.result = result;
    }
}
