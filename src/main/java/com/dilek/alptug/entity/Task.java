package com.dilek.alptug.entity;

import java.io.Serializable;

/**
 * This class represents a task to be offloaded to workers. It contains row and column vectors to be multiplied.
 *
 * @author alptugd
 */
public class Task implements Serializable {
	/**
	 * identifier of the task
	 */
    private int taskId;
    /**
     * a flattened part of the first(row) matrix
     */
    private int [] rowVectors;
    /**
     * a flattened part of the second(column) matrix
     */
    private int [] columnVectors;
    /**
     * length of the vectors to be multiplied
     */
    private int vectorLength;
    /**
     * task result reply address
     */
	private String replyAddress;
	/**
	 * task result reply port
	 */
	private int replyPort;

    public int getVectorLength() {
        return vectorLength;
    }

    public void setVectorLength(int vectorLength) {
        this.vectorLength = vectorLength;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int[] getRowVectors() {
        return rowVectors;
    }

    public void setRowVectors(int[] rowVectors) {
        this.rowVectors = rowVectors;
    }

    public int[] getColumnVectors() {
        return columnVectors;
    }

    public void setColumnVectors(int[] columnVectors) {
        this.columnVectors = columnVectors;
    }

	public String getReplyAddress() {
		return replyAddress;
	}

	public void setReplyAddress(String replyAddress) {
		this.replyAddress = replyAddress;
	}

	public int getReplyPort() {
		return replyPort;
	}

	public void setReplyPort(int replyPort) {
		this.replyPort = replyPort;
	}
}
