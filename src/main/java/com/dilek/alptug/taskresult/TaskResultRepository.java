package com.dilek.alptug.taskresult;

import com.dilek.alptug.entity.TaskResult;

/**
 * Interface representing repository where task results are kept
 * 
 * @author alptugd
 *
 */
public interface TaskResultRepository {
	
	/**
	 * Persists task result to repository
	 * @param taskResult, save task result
	 */
	void persistResult(TaskResult taskResult);
	
	/**
	 * Returns task result with given id
	 * @param taskId identifier of the task result
	 * @return related task result
	 */
	TaskResult getTaskResultByTaskId(int taskId);
}
