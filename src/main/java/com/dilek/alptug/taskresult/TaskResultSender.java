package com.dilek.alptug.taskresult;

import com.dilek.alptug.taskinformation.TaskInformationBuffer;

/**
 * Functional interface to send task results existing in taskinformation.
 *
 * @author alptugd
 */
public interface TaskResultSender {

	void sendTaskResults(TaskInformationBuffer buffer);
}
