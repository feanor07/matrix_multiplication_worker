package com.dilek.alptug.taskresult;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.dilek.alptug.taskinformation.TaskInformationBuffer;
import com.dilek.alptug.datasender.ObjectSender;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskInformation;
import com.dilek.alptug.entity.TaskResult;
import com.dilek.alptug.entity.TaskInformation.TaskStatus;

/**
 * Default implementation for {@link TaskResultSender}. Sends the results within a separate thread
 *
 * @author alptugd
 */
public class DefaultTaskResultSender implements TaskResultSender {

	private ObjectSender<TaskResult> taskResultSender;

	public DefaultTaskResultSender(ObjectSender<TaskResult> taskResultSender) {
		this.taskResultSender = taskResultSender;
	}

	@Override
	public void sendTaskResults(TaskInformationBuffer buffer) {
		List<TaskInformation> calculatedTasks = buffer.getCalculatedTaskInformations();
		calculatedTasks.stream().forEach(taskInformation -> {
			taskInformation.setStatus(TaskStatus.TO_BE_SENT);
			new Thread(() -> {
				TaskResult taskResult = taskInformation.getTaskResult();
				Task task = taskInformation.getTask();
				boolean sendObject = taskResultSender.sendObject(taskResult,
						new InetSocketAddress(task.getReplyAddress(), task.getReplyPort()));
				if(sendObject) {
					buffer.removeTaskInformationByTaskId(taskInformation.getTask().getTaskId());
					Logger.getLogger(DefaultTaskResultSender.class.getName()).log(Level.INFO, "Task with id " + taskInformation.getTask().getTaskId() + " is sent to address: " + task.getReplyAddress() + ":" + task.getReplyPort());
				} else {
				    taskInformation.setStatus(TaskStatus.NEW);
                }
			}).start();
		});
	}
}
