package com.dilek.alptug.taskresult;

import com.dilek.alptug.entity.TaskResult;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * In memory implementation of the {@link TaskResultRepository} interface
 *
 * @author alptugd
 */
public class InMemoryTaskResultRepository implements TaskResultRepository {
	
	private Map<Integer, TaskResult> taskResults = new ConcurrentHashMap<>();
	
	@Override
	public void persistResult(TaskResult taskResult) {
		taskResults.put(taskResult.getTaskId(), taskResult);
	}

	@Override
	public TaskResult getTaskResultByTaskId(int taskId) {
		return taskResults.get(taskId);
	}

}
