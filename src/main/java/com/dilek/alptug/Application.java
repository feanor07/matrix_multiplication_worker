package com.dilek.alptug;

import com.dilek.alptug.taskinformation.DefaultTaskInformationBuffer;
import com.dilek.alptug.taskresult.InMemoryTaskResultRepository;
import com.dilek.alptug.taskinformation.TaskInformationBuffer;
import com.dilek.alptug.taskresult.TaskResultRepository;
import com.dilek.alptug.datareceiver.ObjectReceiver;
import com.dilek.alptug.datareceiver.UDPBasedObjectReceiver;
import com.dilek.alptug.datasender.ObjectSender;
import com.dilek.alptug.datasender.UDPBasedObjectSender;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskInformation;
import com.dilek.alptug.entity.TaskResult;
import com.dilek.alptug.task.DefaultMatrixMultiplier;
import com.dilek.alptug.taskinformation.DefaultTaskInformationBufferProcessor;
import com.dilek.alptug.task.MatrixMultiplier;
import com.dilek.alptug.taskresult.DefaultTaskResultSender;
import com.dilek.alptug.taskresult.TaskResultSender;

import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main thread waiting a port number as program argument in order to open in order to receive tasks. 3 main threads are
 * started; where first one is for retrieving tasks via UDP socket, second one is for processing retrieved tasks, and
 * the last one is for sending task results constructed.
 *
 * @author alptugd
 */
public class Application {

	public static void main(String[] args) throws Exception {
		int port = Integer.valueOf(args[0]);
		ObjectSender<TaskResult> udpBasedObjectSender = new UDPBasedObjectSender<>();
		ObjectReceiver<Task> udpBasedObjectReceiver = new UDPBasedObjectReceiver<>();
		
		TaskResultRepository taskResultRepository = new InMemoryTaskResultRepository();
		TaskInformationBuffer buffer = new DefaultTaskInformationBuffer();
		MatrixMultiplier matrixMultiplier = new DefaultMatrixMultiplier();
		
		DefaultTaskInformationBufferProcessor taskProcessor = new DefaultTaskInformationBufferProcessor(taskResultRepository, matrixMultiplier,
                InetAddress.getLocalHost().getHostAddress(), port);
		TaskResultSender taskResultSender = new DefaultTaskResultSender(udpBasedObjectSender);
		
		startTaskReceiverThread(udpBasedObjectReceiver, buffer, port);
		startTaskProcessorThread(taskProcessor, buffer);
		startTaskResultSenderThread(taskResultSender, buffer);
	}

	private static void startTaskResultSenderThread(TaskResultSender taskResultSender, TaskInformationBuffer buffer) {
		new Thread(()->{
			Logger.getLogger(Application.class.getName()).log(Level.INFO, "Task sender thread started.");
			while(true) {
				taskResultSender.sendTaskResults(buffer);
				sleepCurrentThread();
			}
		}).start();
	}

	private static void startTaskProcessorThread(DefaultTaskInformationBufferProcessor taskProcessor, TaskInformationBuffer buffer) {
		Thread taskProcessorThread = new Thread(()->{
			Logger.getLogger(Application.class.getName()).log(Level.INFO, "Task executor thread started.");
			while(true) {
				taskProcessor.processTasksInBuffer(buffer);
				sleepCurrentThread();
			}
		});
		taskProcessorThread.start();
	}

	private static void sleepCurrentThread() {
		try {
			Thread.sleep(300);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void startTaskReceiverThread(ObjectReceiver<Task> udpBasedObjectReceiver,
			TaskInformationBuffer taskInformationBuffer, int port) {
		new Thread(()->{
			Logger.getLogger(Application.class.getName()).log(Level.INFO, "UDP Server started on port " + port + " to accept tasks.");
			udpBasedObjectReceiver.startReceiving(port, task->{
				Logger.getLogger(Application.class.getName()).log(Level.INFO, "task with id " + task.getTaskId() + " received.");
				TaskInformation taskInformation = new TaskInformation(task);
				taskInformationBuffer.put(taskInformation);
			});
			sleepCurrentThread();
		}).start();
	}
}
